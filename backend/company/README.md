Company-Service
======================
Candidate: Thurston Davis
## To build and run the application: <br />

```
./gradlew bootRun
```
To see if the application is up and running
```
curl localhost:8080/interview/health
```

## To create a company:
<br />Example Request Body<br />
```
{
        "companyName": "hello me",
        "companyType": "svd",
        "natureofBusiness": "svd",
        "incorporatedDate": "2020-11-23",
        "emailAddress": "svds@test.com",
        "phoneNumber": "1234355",
        "address": {
                "addressLine1": "tsdvv",
                "addressLine2": "sdv",
                "city": "tv",
                "state": "tv",
                "postalCode": "tv",
                "countryCode": "tv"
        }
}
```
<br />POST command
```
curl -X POST 'http://localhost:8080/interview/v0/company' \
-H 'Accept: application/json' \
-H 'Content-Type: application/json' \
-d '{
        "companyName": "hello me",
        "companyType": "svd",
        "natureofBusiness": "svd",
        "incorporatedDate": "2020-11-23",
        "emailAddress": "svds@test.com",
        "phoneNumber": "1234355",
        "address": {
                "addressLine1": "tsdvv",
                "addressLine2": "sdv",
                "city": "tv",
                "state": "tv",
                "postalCode": "tv",
                "countryCode": "tv"
        }
}'
```
Example Successful 201 response
```
{
	"id": "de9dea3d-3eed-4ce9-a60d-ef23decdb98d",
	"companyName": "hello me",
	"companyType": "svd",
	"natureofBusiness": "svd",
	"incorporatedDate": "2020-11-23",
	"emailAddress": "svds@test.com",
	"phoneNumber": "1234355",
	"address": {
		"addressLine1": "tsdvv",
		"addressLine2": "sdv",
		"city": "tv",
		"state": "tv",
		"postalCode": "tv",
		"countryCode": "tv"
	},
	"createdTime": "2020-11-23 12:10:57",
	"updatedTime": "2020-11-23 12:10:57"
}
```

