package com.sedex.connect.rest.v1.response

import com.sedex.connect.domain.models.Company
import com.sedex.connect.domain.models.CompanyAddress
import com.sedex.connect.rest.v1.resources.CompanyResource.Companion.dateFormatter
import com.sedex.connect.rest.v1.resources.CompanyResource.Companion.dateTimeFormatter

data class CompanyResponse(
        val id: String,
        val companyName: String,
        val companyType: String,
        val natureofBusiness: String,
        val incorporatedDate: String,
        val emailAddress: String,
        val phoneNumber: String,
        val address: CompanyAddress,
        val createdTime: String,
        val updatedTime: String
) {

    companion object {
        fun fromCompany(company: Company): CompanyResponse {
            return CompanyResponse(
                    company.id.toString(),
                    company.companyName,
                    company.companyType,
                    company.natureOfBusiness,
                    company.incorporatedDate.format(dateFormatter),
                    company.emailAddress,company.phoneNumber,
                    company.address,
                    company.createdTime.format(dateTimeFormatter),
                    dateTimeFormatter.format(company.updatedTime)
            )
        }
    }
}
