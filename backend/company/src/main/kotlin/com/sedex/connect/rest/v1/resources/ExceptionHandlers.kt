package com.sedex.connect.rest.v1.resources

import com.sedex.connect.domain.service.exceptions.DuplicateCompanyException
import com.sedex.connect.rest.v1.response.ErrorListResponse
import org.hibernate.validator.internal.engine.path.PathImpl
import org.springframework.context.annotation.Configuration
import org.springframework.stereotype.Component
import java.util.stream.Collectors
import javax.validation.ConstraintViolation
import javax.validation.ConstraintViolationException
import javax.ws.rs.core.Response
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Configuration
class ExceptionHandlers {

    @Provider
    @Component
    class ConstraintViolationExceptionMapper : ExceptionMapper<ConstraintViolationException>{

        override fun toResponse(exception: ConstraintViolationException?): Response {
            return Response.status(400)
                    .entity(exception?.let { generateErrorResponse(it) })
                    .build()
        }

        private fun generateErrorResponse (exception: ConstraintViolationException) : ErrorListResponse{
            return ErrorListResponse(exception.constraintViolations.stream()
                    .map { v -> ErrorListResponse.ErrorResponse(getProperty(v), v.message) }
                    .collect(Collectors.toList()))

        }

        private fun getProperty(v: ConstraintViolation<*>) : String {
            var path: PathImpl = v.propertyPath as PathImpl
            return "invalid.${path.leafNode.toString()}"
        }

    }

    @Provider
    @Component
    class DuplicateCompanyExceptionMapper : ExceptionMapper<DuplicateCompanyException>{
        override fun toResponse(exception: DuplicateCompanyException?): Response {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(exception?.message?.let { ErrorListResponse("duplicate.company", it) })
                    .build()
        }
    }
}