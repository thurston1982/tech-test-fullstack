package com.sedex.connect.infrastructure.dao

import com.sedex.connect.domain.CompanyRepository
import com.sedex.connect.domain.models.Company
import org.springframework.stereotype.Component
import java.util.concurrent.ConcurrentHashMap

@Component
class CompanyRepositoryBasicImpl : CompanyRepository {
    var companies: ConcurrentHashMap<String, Company> = ConcurrentHashMap()

    override fun getCompanyByCompanyName(companyName: String): Company? {
        return companies.get(companyName)
    }

    override fun storeCompany(company: Company) {
        companies.putIfAbsent(company.companyName, company)
    }
}