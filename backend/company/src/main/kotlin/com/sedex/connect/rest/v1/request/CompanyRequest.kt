package com.sedex.connect.rest.v1.request

import com.sedex.connect.rest.v1.validators.DateFormatter
import javax.validation.constraints.NotEmpty

data class CompanyRequest (
        @get:NotEmpty(message ="message is required") val companyName: String,
        val companyType: String,
        val natureofBusiness: String,
        @get:DateFormatter(message = "invalid incorporatedDate") val incorporatedDate: String,
        val emailAddress: String,
        val phoneNumber: String,
        var address: CompanyAddressRequest
)