package com.sedex.connect.rest.v1.resources

import com.sedex.connect.domain.service.CompanyService
import com.sedex.connect.rest.v1.request.CompanyRequest
import com.sedex.connect.rest.v1.response.CompanyResponse
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import javax.validation.Valid
import javax.ws.rs.Consumes
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Component
@Path("/v0")
class CompanyResource(val companyService: CompanyService) {

    companion object {
        val dateFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        val dateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
    }

    @POST
    @Path("/company")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    fun createCompany(@Valid companyRequest: CompanyRequest): Response {
        val company = companyService.createCompany(
            companyName = companyRequest.companyName,
            companyType = companyRequest.companyType,
            natureofBusiness = companyRequest.natureofBusiness,
            incorporatedDate = convertToLocalDate(companyRequest.incorporatedDate),
            emailAddress = companyRequest.emailAddress,
            phoneNumber = companyRequest.phoneNumber,
            address = companyRequest.address.toCompanyAddress()
        )
        return Response.status(HttpStatus.CREATED.value())
            .entity(CompanyResponse.fromCompany(company))
            .build()
    }

    private fun convertToLocalDate(dateTime:String) : LocalDate {
        return LocalDate.parse(dateTime, dateFormatter)
    }
}