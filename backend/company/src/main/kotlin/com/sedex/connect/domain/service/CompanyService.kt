package com.sedex.connect.domain.service

import com.sedex.connect.domain.models.Company
import com.sedex.connect.domain.models.CompanyAddress
import java.time.LocalDate

interface CompanyService {
    fun createCompany(companyName: String, companyType: String, natureofBusiness: String, incorporatedDate: LocalDate, emailAddress: String, phoneNumber: String, address: CompanyAddress) : Company
}