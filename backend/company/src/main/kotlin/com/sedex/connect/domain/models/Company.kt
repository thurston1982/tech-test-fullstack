package com.sedex.connect.domain.models

import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

data class Company (
        val id: UUID,
        val companyName: String,
        val companyType: String,
        val natureOfBusiness: String,
        val incorporatedDate: LocalDate,
        val emailAddress: String,
        val phoneNumber: String,
        val address: CompanyAddress,
        val createdTime: LocalDateTime,
        val updatedTime: LocalDateTime
)