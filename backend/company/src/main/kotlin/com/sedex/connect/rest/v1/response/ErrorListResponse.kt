package com.sedex.connect.rest.v1.response


class ErrorListResponse {
   var errors: ArrayList<ErrorResponse> = ArrayList()

    constructor(code: String, message: String) {
        this.errors.add(ErrorResponse(code, message))
    }

    constructor(errors: List<ErrorResponse>){
        this.errors.addAll(errors)
    }

    data class ErrorResponse(val code: String, val message: String)

}