package com.sedex.connect.domain.service.exceptions

import java.lang.RuntimeException

class DuplicateCompanyException(msg: String?) : RuntimeException(msg) {
}