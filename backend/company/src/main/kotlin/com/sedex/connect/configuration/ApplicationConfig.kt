package com.sedex.connect.configuration

import com.sedex.connect.domain.CompanyRepository
import com.sedex.connect.domain.service.CompanyService
import com.sedex.connect.domain.service.CompanyServiceImpl
import com.sedex.connect.infrastructure.dao.CompanyRepositoryBasicImpl
import com.sedex.connect.rest.v1.resources.CompanyResource
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class ApplicationConfig {

    @Bean
    fun companyResource(companyService: CompanyService): CompanyResource {
        return CompanyResource(companyService)
    }

    @Bean
    fun companyRepository() : CompanyRepository {
        return CompanyRepositoryBasicImpl()
    }

    @Bean
    fun companyService(companyRepository: CompanyRepository) : CompanyService {
        return CompanyServiceImpl(companyRepository)
    }

}