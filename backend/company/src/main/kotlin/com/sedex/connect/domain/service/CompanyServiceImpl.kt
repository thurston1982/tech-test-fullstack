package com.sedex.connect.domain.service

import com.sedex.connect.domain.CompanyRepository
import com.sedex.connect.domain.models.Company
import com.sedex.connect.domain.models.CompanyAddress
import com.sedex.connect.domain.service.exceptions.DuplicateCompanyException
import com.sedex.connect.rest.v1.resources.CompanyResource
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

@Service
class CompanyServiceImpl(val companyRepository: CompanyRepository) : CompanyService {

    companion object {
        private val logger = LoggerFactory.getLogger(CompanyResource::class.java)
    }

    override fun createCompany(companyName: String, companyType: String, natureofBusiness: String, incorporatedDate: LocalDate, emailAddress: String, phoneNumber: String, address: CompanyAddress): Company {
        companyRepository.getCompanyByCompanyName(companyName)?:let {
            val company = createCompany(createCompanyUUID(), companyName, companyType, natureofBusiness, incorporatedDate, emailAddress, phoneNumber, address)
            companyRepository.storeCompany(company)
            return company
        }
        logger.error("Company Already exist, companyName={}", companyName)
        throw DuplicateCompanyException("Company Already exist: $companyName")
    }

    private fun createCompany(id : UUID, companyName: String, companyType: String, natureofBusiness: String, incorporatedDate: LocalDate, emailAddress: String, phoneNumber: String, address: CompanyAddress) : Company{
        return Company(id, companyName,companyType,natureofBusiness,incorporatedDate,emailAddress,phoneNumber,address, LocalDateTime.now(), LocalDateTime.now())
    }

    private fun createCompanyUUID() : UUID {
        return UUID.randomUUID()
    }
}