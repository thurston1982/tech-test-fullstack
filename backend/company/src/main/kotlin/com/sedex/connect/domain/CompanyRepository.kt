package com.sedex.connect.domain

import com.sedex.connect.domain.models.Company

interface CompanyRepository {
    fun getCompanyByCompanyName(companyName : String) : Company?
    fun storeCompany(company : Company)
}