package com.sedex.connect.configuration

import org.glassfish.jersey.server.ResourceConfig
import org.glassfish.jersey.server.ServerProperties
import org.springframework.context.annotation.Configuration

@Configuration
class JerseyConfig : ResourceConfig() {
    init {
        packages("com.sedex.connect.rest.v1.resources")
        property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true)
    }
}