package com.sedex.connect.application

import com.sedex.connect.configuration.ApplicationConfig
import com.sedex.connect.configuration.JerseyConfig
import com.sedex.connect.rest.v1.resources.ExceptionHandlers
import lombok.extern.slf4j.Slf4j
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.Import

@Slf4j
@Import(ApplicationConfig::class, JerseyConfig::class, ExceptionHandlers::class)
@EnableAutoConfiguration
class CompanyApplication {

}

fun main(args: Array<String>) {
	SpringApplication.run(CompanyApplication::class.java, *args)
}
