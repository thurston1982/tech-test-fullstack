package com.sedex.connect.rest.v1.request

import com.sedex.connect.domain.models.CompanyAddress

data class CompanyAddressRequest(
        val addressLine1: String,
        val addressLine2: String,
        val city: String,
        val state: String,
        val postalCode: String,
        val countryCode: String
)
{
    fun toCompanyAddress() : CompanyAddress {
        return CompanyAddress(this.addressLine1,this.addressLine2,this.city, this.state, this.postalCode, this.countryCode)
    }
}