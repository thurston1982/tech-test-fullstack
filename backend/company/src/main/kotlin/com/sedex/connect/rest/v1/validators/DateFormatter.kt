package com.sedex.connect.rest.v1.validators

import com.sedex.connect.rest.v1.resources.CompanyResource.Companion.dateFormatter
import java.time.LocalDate
import java.time.format.DateTimeParseException
import javax.validation.Constraint
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import javax.validation.Payload
import kotlin.reflect.KClass


@Target(AnnotationTarget.FIELD, AnnotationTarget.PROPERTY_GETTER)
@MustBeDocumented
@Constraint(validatedBy = [DateFormatterValidator::class])
annotation class DateFormatter(
        val message: String = "invalid date",
        val groups: Array<KClass<*>> = [],
        val payload: Array<KClass<out Payload>> = []
)

class DateFormatterValidator : ConstraintValidator<DateFormatter, String> {
    override fun isValid(value: String?, context: ConstraintValidatorContext?): Boolean {
        if (value == null) return true
        var status = true
        try {
            LocalDate.parse(value, dateFormatter)
        } catch (e: DateTimeParseException) {
            status = false
        }
        return status
    }
}