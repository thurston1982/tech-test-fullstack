package com.sedex.connect.rest.v1.request

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import javax.validation.ConstraintViolation
import javax.validation.Validation
import javax.validation.Validator

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CompanyRequestSmallTest{
    lateinit var validator : Validator
    val companyAddressRequest = CompanyAddressRequest(addressLine1 = "123", addressLine2 = "sfsf", city = "London", countryCode = "London", postalCode = "n42ga", state = "1234")

    @BeforeAll
    internal fun setupValidatorInstance(){
        validator = Validation.buildDefaultValidatorFactory().validator
    }

    @Test
    fun testWhenCompanyRequestHasEmptyCompanyName_thenViolationsShouldBeReported() {
        val companyRequest = CompanyRequest("","legal","corporation-tax","2020-10-21","test@gmail.com","012324", companyAddressRequest)

        val violations: Set<ConstraintViolation<CompanyRequest>> = validator.validate(companyRequest)
        assertThat(violations.size).isEqualTo(1)
        assertThat(violations).anyMatch { violation -> "message is required".equals(violation.message) }
    }

    @Test
    fun testWhenCompanyRequestHasInvalidDate_thenViolationsShouldBeReported(){
        val companyRequest = CompanyRequest("sss","legal","corporation-tax","20scvs","test@gmail.com","012324", companyAddressRequest)

        val violations: Set<ConstraintViolation<CompanyRequest>> = validator.validate(companyRequest)
        assertThat(violations.size).isEqualTo(1)
        assertThat(violations).anyMatch { violation -> "invalid incorporatedDate".equals(violation.message) }
    }

}