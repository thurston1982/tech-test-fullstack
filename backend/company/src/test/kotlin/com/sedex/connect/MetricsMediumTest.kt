package com.sedex.connect

import io.restassured.RestAssured
import io.restassured.http.ContentType
import org.hamcrest.Matchers
import org.junit.Test

class MetricsMediumTest : AbstractMediumTest() {
    @Test
    fun testHealthCheck(){
        RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .`when`()["/health"]
                .then().log().all()
                .assertThat().statusCode(200).contentType(ContentType.JSON)
                .assertThat().body(Matchers.notNullValue())
                .assertThat().body("status", Matchers.equalTo("UP"))
    }

    @Test
    fun testInfo() {
        RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .`when`()["/info"]
                .then().log().all()
                .assertThat().statusCode(200).contentType(ContentType.JSON)
    }
}