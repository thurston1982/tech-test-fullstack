package com.sedex.connect

import com.sedex.connect.rest.v1.request.CompanyAddressRequest
import com.sedex.connect.rest.v1.request.CompanyRequest
import com.sedex.connect.rest.v1.resources.CompanyResource.Companion.dateFormatter
import com.sedex.connect.rest.v1.response.CompanyResponse
import org.junit.Assert.assertNotNull
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import org.hamcrest.Matchers.notNullValue

import io.restassured.RestAssured
import io.restassured.http.ContentType
import org.hamcrest.Matchers.equalTo
import org.json.JSONObject
import org.junit.Test
import java.time.LocalDate

class CompanyApplicationMediumTest : AbstractMediumTest() {
	val om = ObjectMapper().registerModule(KotlinModule())
	val companyAddress = CompanyAddressRequest(addressLine1 = "line1",
			addressLine2 = "line2",
			city ="city",
			state="state",
			postalCode = "1234",
			countryCode = "123")

	val company = CompanyRequest(companyName = "law-firm",
			companyType = "law",
			natureofBusiness = "tax",
			incorporatedDate = LocalDate.now().format(dateFormatter),
			emailAddress= "test@google.com",
			phoneNumber = "01234",
			address = companyAddress)

	@Test
	fun createCompany_UniqueCompany_CreatedResource() {
		val response = RestAssured.given().log().all()
				.body(om.writeValueAsString(company))
				.accept(ContentType.JSON)
				.contentType(ContentType.JSON)
				.expect()
				.statusCode(201)
				.`when`()
				.post("/v0/company")
				.andReturn()

		val jsonObject = JSONObject(response.body.asString())

		val actualCompanyResponse = om.readValue<CompanyResponse>(jsonObject.toString())
		assertNotNull(actualCompanyResponse)
		assertNotNull(actualCompanyResponse.id)
	}

	@Test
	fun createCompany_DuplicateCompany_InternalServerErrorResponse(){
		RestAssured.given().log().all()
				.body(om.writeValueAsString(company))
				.accept(ContentType.JSON)
				.contentType(ContentType.JSON)
				.expect()
				.statusCode(201)
				.`when`()
				.post("/v0/company")
				.andReturn()

		RestAssured.given().log().all()
				.body(om.writeValueAsString(company))
				.accept(ContentType.JSON)
				.contentType(ContentType.JSON)
				.`when`()
				.post("/v0/company").then().log().all()
				.assertThat().statusCode(500).contentType(ContentType.JSON)
				.assertThat().body(notNullValue())
				.assertThat().body("errors[0].code", equalTo("duplicate.company"))
	}

	@Test
	fun createCompany_EmptyCompanyName_BadRequestResponse(){
		val emptyCompanyName = CompanyRequest(companyName = "",
				companyType = "law",
				natureofBusiness = "tax",
				incorporatedDate = LocalDate.now().format(dateFormatter),
				emailAddress= "test@google.com",
				phoneNumber = "01234",
				address = companyAddress)

		RestAssured.given().log().all()
				.body(om.writeValueAsString(emptyCompanyName))
				.accept(ContentType.JSON)
				.contentType(ContentType.JSON)
				.`when`()
				.post("/v0/company").then().log().all()
				.assertThat().statusCode(400).contentType(ContentType.JSON)
				.assertThat().body(notNullValue())
				.assertThat().body("errors[0].code", equalTo("invalid.companyName"))
	}

	@Test
	fun createCompany_InvalidIncorporatedDate_BadRequestResponse(){
		val invalidIncorporatedDate = CompanyRequest(companyName = "test",
				companyType = "law",
				natureofBusiness = "tax",
				incorporatedDate = "12435",
				emailAddress= "test@google.com",
				phoneNumber = "01234",
				address = companyAddress)

		RestAssured.given().log().all()
				.body(om.writeValueAsString(invalidIncorporatedDate))
				.accept(ContentType.JSON)
				.contentType(ContentType.JSON)
				.`when`()
				.post("/v0/company").then().log().all()
				.assertThat().statusCode(400).contentType(ContentType.JSON)
				.assertThat().body(notNullValue())
				.assertThat().body("errors[0].code", equalTo("invalid.incorporatedDate"))

	}

}
