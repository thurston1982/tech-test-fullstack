package com.sedex.connect.domain.service

import com.sedex.connect.domain.CompanyRepository
import com.sedex.connect.domain.models.Company
import com.sedex.connect.domain.models.CompanyAddress
import com.sedex.connect.domain.service.exceptions.DuplicateCompanyException
import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.*

import org.junit.jupiter.api.Assertions.*
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

class CompanyServiceImplSmallTest {

    lateinit var testObj: CompanyService
    lateinit var companyRepositoryMock: CompanyRepository
    val companyName = "testComp"
    val companyType = "testComp"
    val incorporatedDate = "legal"
    val natureofBusiness = "legal"
    val emailAddress = "xxx@test.com"
    val phoneNumber = "01234567"
    val companyAddress = CompanyAddress("addLine1", "addLine2","London","123","n4 2ga","London")

    @BeforeEach
    fun setUp() {
        companyRepositoryMock = mock()
        testObj = CompanyServiceImpl(companyRepositoryMock)
    }

    @Test
    fun createCompany_UniqueCompany_Company() {
        whenever(companyRepositoryMock.getCompanyByCompanyName("testComp")).thenReturn(null)

        val actualCompany = testObj.createCompany(companyName = companyName, companyType = companyType, natureofBusiness = natureofBusiness, incorporatedDate = LocalDate.now(), emailAddress = emailAddress, phoneNumber = phoneNumber, address = companyAddress)
        assertNotNull(actualCompany)
        assertNotNull(actualCompany.id)
        verify(companyRepositoryMock, times(1)).storeCompany(any())
    }

    @Test
    fun createCompany_CompanyAlreadyExists_Exception(){
        val existingCompany = Company(UUID.randomUUID(),
                companyName = companyName,
                companyType = companyType,
                natureOfBusiness = natureofBusiness,
                incorporatedDate = LocalDate.now(),
                emailAddress = emailAddress,
                phoneNumber = phoneNumber,
                address = companyAddress,
                createdTime = LocalDateTime.now(),
                updatedTime = LocalDateTime.now())

        whenever(companyRepositoryMock.getCompanyByCompanyName(companyName)).thenReturn(existingCompany)
        val duplicateException = assertThrows<DuplicateCompanyException> ("Should throw a duplicateCompanyException"){
            testObj.createCompany(companyName, companyType, natureofBusiness, LocalDate.now(), emailAddress, phoneNumber, companyAddress)
        }
        assertNotNull(duplicateException)
    }
}